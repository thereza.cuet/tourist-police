package com.android.cheffy.injection.component

import com.android.cheffy.injection.module.NetworkModule
import com.android.touristpolice.viewmodels.MainActivityListViewModel
import com.android.touristpolice.viewmodels.TouristSpotListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified NewOnCheffytListViewModel.
     * @param postListViewModel NewOnCheffytListViewModel in which to inject the dependencies
     */
    fun inject(touristSpotViewModel: TouristSpotListViewModel)
    fun inject(homeItem: MainActivityListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun networkModule(networkModule: NetworkModule): Builder
    }
}