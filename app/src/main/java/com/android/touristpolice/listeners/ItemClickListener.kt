package com.android.touristpolice.listeners

import android.view.View

interface ItemClickListener {
    fun onItemClickListener(view: View, position : Number)
}