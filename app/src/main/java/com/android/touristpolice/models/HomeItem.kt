package com.android.touristpolice.models

data class HomeItem (
    val title : String,
    val iconUrl : Number
)