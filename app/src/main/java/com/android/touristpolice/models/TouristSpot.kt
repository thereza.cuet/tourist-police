package com.android.touristpolice.models

data class TouristSpot(
    val id : Number,
    val name : String,
    val descriptions : String,
    val img_url : String
)