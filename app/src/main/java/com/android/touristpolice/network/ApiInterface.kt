package com.android.cheffy.network

import com.android.touristpolice.models.TouristSpot
import retrofit2.http.GET
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiInterface {

    /**
     * Get the list of the pots from the API
     */

//    @POST("user/login")
//    fun userLogin(@Body info: LoginInfo) : Observable<LoginResponse>
//
    @GET("food_json")
    fun getTouristSpot(): Observable<List<TouristSpot>>
}