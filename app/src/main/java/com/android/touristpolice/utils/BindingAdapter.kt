package com.android.cheffy.utils

import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.android.cheffy.utils.extension.getParentActivity
import com.bumptech.glide.Glide
import android.R
import android.text.TextWatcher
import android.widget.ImageView
import com.google.android.material.textfield.TextInputLayout


@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View,  visibility: MutableLiveData<Int>?) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    if(parentActivity != null && visibility != null) {
        visibility.observe(parentActivity, Observer { value -> view.visibility = value?:View.VISIBLE})
    }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    if(parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value?:""})
    }
}

@BindingAdapter("addTextChangeListener")
fun addTextChangeListener(view:EditText, textWatcher: TextWatcher ) {
    view.addTextChangedListener(textWatcher)
}

@BindingAdapter("setError")
fun setErrorText(view:TextInputLayout , textInputLayout: TextInputLayout){
    view.error = textInputLayout.toString()
}

@BindingAdapter("loadImage")
fun loadImage(view: ImageView, logoUrl: String?) {
    if (logoUrl == null) {
        view.setImageResource(R.mipmap.sym_def_app_icon)
    } else {
        Glide.with(view.context).load(logoUrl).into(view)
    }
}

@BindingAdapter("loadImageIcon")
fun loadImageIcon(view: ImageView, logoUrl: Number?) {
    if (logoUrl == null) {
        view.setImageResource(R.mipmap.sym_def_app_icon)
    } else {
        Glide.with(view.context).load(logoUrl).into(view)
    }
}