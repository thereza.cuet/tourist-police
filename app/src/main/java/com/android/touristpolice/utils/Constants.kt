package com.android.cheffy.utils

class Constants {

    companion object {

        /** The base URL of the API */
        //const val BASE_URL: String = "https://raw.githubusercontent.com/therezacuet/Project-Report/master/"
        const val BASE_URL : String = "https://api.thecheffy.com/"
        const val BUNDLE_KEY_CATEGORY_ID = "cat_id"
        const val BUNDLE_KEY_SEARCH_TEXT = "search_query"
        const val DEFAULT_CATEGORY_ID = -1
    }
}