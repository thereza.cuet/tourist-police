package com.android.touristpolice.base

import androidx.lifecycle.ViewModel
import com.android.cheffy.injection.component.DaggerViewModelInjector
import com.android.cheffy.injection.component.ViewModelInjector
import com.android.cheffy.injection.module.NetworkModule
import com.android.touristpolice.viewmodels.MainActivityListViewModel
import com.android.touristpolice.viewmodels.TouristSpotListViewModel


abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is TouristSpotListViewModel -> injector.inject(this)
            is MainActivityListViewModel -> injector.inject(this)
        }
    }
}