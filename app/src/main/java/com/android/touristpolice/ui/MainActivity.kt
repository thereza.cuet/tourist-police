package com.android.touristpolice.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.android.touristpolice.R
import com.android.touristpolice.viewmodels.MainActivityListViewModel
import com.android.touristpolice.databinding.ActivityMainBinding
import com.android.touristpolice.ui.utils.GridItemDecoration
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding
    lateinit var viewModel : MainActivityListViewModel
    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        initVariable()
        initView()
        initListener()
        initFunctionality()
    }

    private fun initVariable() {

    }

    private fun initView() {

        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        binding.rvHome.layoutManager = GridLayoutManager(this,3)
        binding.rvHome.addItemDecoration(GridItemDecoration(0,3))

        viewModel = ViewModelProviders.of(this).get(MainActivityListViewModel::class.java)
        binding.viewModel = viewModel

    }

    private fun initListener() {

    }

    private fun initFunctionality() {

    }
}
