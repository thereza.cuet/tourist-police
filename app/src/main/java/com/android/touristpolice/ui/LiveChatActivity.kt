package com.android.touristpolice.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.touristpolice.R

class LiveChatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_chat)
    }
}
