package com.android.touristpolice.viewmodels

import androidx.lifecycle.MutableLiveData
import com.android.touristpolice.base.BaseViewModel
import com.android.touristpolice.models.TouristSpot

class TouristSpotViewModel : BaseViewModel(){

    private val spotTitle = MutableLiveData<String>()
    private val spotDescription = MutableLiveData<String>()
    private val spotImgUrl = MutableLiveData<String>()

    fun bind(post: TouristSpot){
        spotTitle.value = post.name
        spotDescription.value = post.descriptions
        spotImgUrl.value = post.img_url
    }

    fun getSpotTitle():MutableLiveData<String>{
        return spotTitle
    }

    fun getSpotDescription():MutableLiveData<String>{
        return spotDescription
    }

    fun getSpotImgUrl():MutableLiveData<String>{
        return spotImgUrl
    }

}