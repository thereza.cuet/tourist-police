package com.android.touristpolice.viewmodels

import com.android.touristpolice.R
import com.android.touristpolice.adapters.MainActivityAdapter
import com.android.touristpolice.adapters.TouristSpotAdapter
import com.android.touristpolice.base.BaseViewModel
import com.android.touristpolice.models.HomeItem
import com.bumptech.glide.Glide.init
import javax.inject.Inject

class MainActivityListViewModel : BaseViewModel(){

    val mAdapter: MainActivityAdapter = MainActivityAdapter()
    val dataList = ArrayList<HomeItem>()

    init{
        loadPosts()
    }

    private fun loadPosts(){

        dataList.add(HomeItem("Help/Complain", R.drawable.ic_complain))
        dataList.add(HomeItem("Notice/News", R.drawable.help))
        dataList.add(HomeItem("Tourist Spot", R.drawable.ic_beach))
        dataList.add(HomeItem("About Us", R.drawable.help))
        dataList.add(HomeItem("FAQ", R.drawable.help))
        dataList.add(HomeItem("Contact", R.drawable.help))
        dataList.add(HomeItem("Find Us", R.drawable.help))
        dataList.add(HomeItem("Laws & Rules", R.drawable.help))
        dataList.add(HomeItem("Forms", R.drawable.help))
        dataList.add(HomeItem("Live Chat", R.drawable.help))
        dataList.add(HomeItem("Social Network", R.drawable.help))
        dataList.add(HomeItem("Hotline", R.drawable.help))
        mAdapter.updatePostList(dataList)
    }
}