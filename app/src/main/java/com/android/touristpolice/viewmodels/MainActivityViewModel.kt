package com.android.touristpolice.viewmodels

import androidx.lifecycle.MutableLiveData
import com.android.touristpolice.base.BaseViewModel
import com.android.touristpolice.models.HomeItem

class MainActivityViewModel : BaseViewModel() {

    private val itemTitle = MutableLiveData<String>()
    private val itemIcon = MutableLiveData<Number>()

    fun bind(data: HomeItem){
        itemTitle.value = data.title
        itemIcon.value = data.iconUrl
    }

    fun getItemTitle(): MutableLiveData<String>{
        return itemTitle
    }

    fun getItemIcon() : MutableLiveData<Number>{
        return itemIcon
    }

}