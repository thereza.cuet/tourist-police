package com.android.touristpolice.viewmodels

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.android.cheffy.network.ApiInterface
import com.android.touristpolice.R
import com.android.touristpolice.adapters.TouristSpotAdapter
import com.android.touristpolice.base.BaseViewModel
import com.android.touristpolice.models.TouristSpot
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TouristSpotListViewModel : BaseViewModel() {

    @Inject
    lateinit var apiInterface: ApiInterface
    private lateinit var subscription: Disposable
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPosts() }

    val mAdapter: TouristSpotAdapter = TouristSpotAdapter()

    init{
        loadPosts()
    }

    private fun loadPosts(){
        subscription = apiInterface.getTouristSpot()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe(
                // Add result
                { result -> onRetrievePostListSuccess(result) },
                { onRetrievePostListError() }
            )
    }

    private fun onRetrievePostListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePostListSuccess(postList:List<TouristSpot>){
        mAdapter.updatePostList(postList)
    }

    private fun onRetrievePostListError(){
        errorMessage.value = R.string.post_error
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}