package com.android.touristpolice.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.touristpolice.R
import com.android.touristpolice.databinding.ItemHomeBinding
import com.android.touristpolice.listeners.ItemClickListener
import com.android.touristpolice.models.HomeItem
import com.android.touristpolice.viewmodels.MainActivityViewModel

class MainActivityAdapter : RecyclerView.Adapter<MainActivityAdapter.ViewHolder>() {
    private lateinit var dataList:List<HomeItem>

    private lateinit var mListener : ItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainActivityAdapter.ViewHolder {
        val binding: ItemHomeBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_home, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainActivityAdapter.ViewHolder, position: Int) {

        holder.bind(dataList[position])
        holder.itemView.setOnClickListener(View.OnClickListener {
            //val intent = Intent(holder.itemView.context,FoodDetailsActivity::class.java)
            //holder.itemView.context.startActivity(intent)
            mListener.onItemClickListener(holder.itemView,position)
        })

    }

    override fun getItemCount(): Int {
        return if(::dataList.isInitialized) dataList.size else 0
    }

    fun updatePostList(dataList:List<HomeItem>){
        this.dataList = dataList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemHomeBinding): RecyclerView.ViewHolder(binding.root){
        private val viewModel = MainActivityViewModel()
        fun bind(data: HomeItem){
            viewModel.bind(data)
            binding.viewModel = viewModel
        }
    }

    fun setItemClickListener(mListener : ItemClickListener){
        if (mListener != null){
            this.mListener = mListener
        }
    }
}