package com.android.touristpolice.adapters

import android.content.Intent
import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.touristpolice.R
import com.android.touristpolice.models.TouristSpot
import com.android.touristpolice.viewmodels.TouristSpotViewModel
import com.android.touristpolice.databinding.ItemTouristSpotBinding;
import com.android.touristpolice.listeners.ItemClickListener

class TouristSpotAdapter() : RecyclerView.Adapter<TouristSpotAdapter.ViewHolder>() {

    private lateinit var dataList:List<TouristSpot>

    private lateinit var mListener : ItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TouristSpotAdapter.ViewHolder {
        val binding: ItemTouristSpotBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_tourist_spot, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TouristSpotAdapter.ViewHolder, position: Int) {

        holder.bind(dataList[position])
        holder.itemView.setOnClickListener(View.OnClickListener {
            //val intent = Intent(holder.itemView.context,FoodDetailsActivity::class.java)
            //holder.itemView.context.startActivity(intent)
            mListener.onItemClickListener(holder.itemView,position)
        })

    }

    override fun getItemCount(): Int {
        return if(::dataList.isInitialized) dataList.size else 0
    }

    fun updatePostList(dataList:List<TouristSpot>){
        this.dataList = dataList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemTouristSpotBinding): RecyclerView.ViewHolder(binding.root){
        private val viewModel = TouristSpotViewModel()
        fun bind(data:TouristSpot){
            viewModel.bind(data)
            binding.viewModel = viewModel
        }
    }

    fun setItemClickListener(mListener : ItemClickListener){
        if (mListener != null){
            this.mListener = mListener
        }
    }
}